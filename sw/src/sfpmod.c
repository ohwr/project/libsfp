/*
 * sfpmod.c
 *
 *  Created on: 26 jun. 2017
 *      Author: vincentb
 */


#include "sfpmod.h"
#include <stdbool.h>
#include "sfpmod_prov.h"

#define SFP_REG_PAGE	127

sfp_status_t sfp_mod_get(unsigned int idx, sfp_mod_t ** module)
{
	return _sfp_mod_get(idx, module);
}

unsigned int sfp_mod_count()
{
	return _sfp_mod_count();
}


sfp_status_t sfp_mod_ee_rd(sfp_mod_t * mod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count)
{
	sfp_prov_mod_t * pmod = (sfp_prov_mod_t *)mod;
	return pmod->ee_rd(pmod, aspace, data, reg, count);
}


sfp_status_t sfp_mod_ee_wr(sfp_mod_t * mod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count)
{
	sfp_prov_mod_t * pmod = (sfp_prov_mod_t *)mod;
	return pmod->ee_wr(pmod, aspace, data, reg, count);
}

sfp_status_t sfp_mod_status(sfp_mod_t * mod, uint32_t * flags)
{
	sfp_prov_mod_t * pmod = (sfp_prov_mod_t *)mod;
	return pmod->sts(pmod, flags);
}



// -----------------------------------------------------------------------------------------------------
//  Composite provider interface
// -----------------------------------------------------------------------------------------------------


sfp_status_t sfp_comp_mod_set_mux(sfp_comp_mod_t * cmod, sfp_mux_op_t * op)
{
	uint8_t t;
	sfp_i2c_slave_t slave;
	switch (op->any.typ)
	{
	case SFP_MUX_TYP_I2C:
		// I2C Mux switch
		switch (op->i2c.part)
		{
		case SFP_MUX_PART_PCA9548:
			// set the I2C mux to the right position
			t = 1 << op->i2c.pos;


			slave = sfp_i2c_slave(cmod->i2c, op->i2c.addr);

			return sfp_i2c_wr(&slave, &t, 1);
		default:
			break;
		}
		/* no break */
		default:
			return SFP_STATUS_MODULE_CONFIG_ERROR;
	}
	return SFP_STATUS_OK;
}

sfp_status_t sfp_comp_mod_set_muxes(sfp_comp_mod_t * cmod)
{
	sfp_status_t sts = SFP_STATUS_OK;

	if (cmod->mux0.val != 0)
	{
		sts = sfp_comp_mod_set_mux(cmod, &cmod->mux0);
		if (sts != SFP_STATUS_OK) return sts;
	}

	if (cmod->mux1.val != 0) sts = sfp_comp_mod_set_mux(cmod, &cmod->mux1);
	return sts;
}


sfp_status_t sfp_comp_mod_ee_rd(struct sfp_prov_mod_s * pmod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count)
{
	sfp_comp_mod_t * cmod  = (sfp_comp_mod_t *)pmod;

	sfp_status_t sts = sfp_i2c_mtx(cmod->i2c, SFP_I2C_MUX_LOCK);
	if (sts != SFP_STATUS_OK) return sts;

	sts = sfp_comp_mod_set_muxes(cmod);
	if (sts != SFP_STATUS_OK) goto finally;


	// get slave instance.
	sfp_i2c_slave_t slave = sfp_i2c_slave(cmod->i2c, SFP_AS2ADDR(aspace));
	// if these is a page mask, set it
	if (aspace & SFP_AS_PAGEMASK)
	{
		uint8_t page = SFP_AS2PAGE(aspace);
		sts = sfp_i2c_a8_wr(&slave, SFP_REG_PAGE, &page, 1);
		if (sts != SFP_STATUS_OK) goto finally;
	}

	sts = sfp_i2c_a8_rd(&slave, reg, data, count);

finally:
	sfp_i2c_mtx(cmod->i2c, SFP_I2C_MUX_UNLOCK);
	return sts;
}

#ifdef SFP_WRITE_SUPPORT
sfp_status_t sfp_comp_mod_ee_wr(struct sfp_prov_mod_s * pmod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count)
{
	sfp_comp_mod_t * cmod  = (sfp_comp_mod_t *)pmod;

	sfp_status_t sts = sfp_i2c_mtx(cmod->i2c, SFP_I2C_MUX_LOCK);
	if (sts != SFP_STATUS_OK) return sts;

	sts = sfp_comp_mod_set_muxes(cmod);
	if (sts != SFP_STATUS_OK) goto finally;


	sfp_i2c_slave_t slave = sfp_i2c_slave(cmod->i2c, SFP_AS2ADDR(aspace));
	// if these is a page mask, set it
	if (aspace & SFP_AS_PAGEMASK)
	{
		uint8_t page = SFP_AS2PAGE(aspace);
		sts = sfp_i2c_a8_wr(&slave, SFP_REG_PAGE, &page, 1);
		if (sts != SFP_STATUS_OK) goto finally;
	}

	if (reg % 8 == 0 && count == 8)
	{
		// simple case, write one register with offset of 8 and with 8 bytes
		sts = sfp_i2c_a8_wr(&slave, reg, data, count);
	} else {
		// complex writing is not supported yet
		return	SFP_STATUS_NOT_SUPPORTED;
	}

finally:
	sfp_i2c_mtx(cmod->i2c, SFP_I2C_MUX_UNLOCK);
	return sts;
}

#else

sfp_status_t sfp_comp_mod_ee_wr(struct sfp_prov_mod_s * pmod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count)
{
	return SFP_STATUS_NOT_SUPPORTED;
}

#endif


sfp_status_t sfp_comp_mod_sts  (struct sfp_prov_mod_s * pmod, uint32_t * flags)
{
	sfp_comp_mod_t * cmod  = (sfp_comp_mod_t *)pmod;

	uint32_t flags_;

	// If all GPIOs are defined
	if (cmod->rx_loss != SFP_GPIO_NONE && cmod->tx_fault != SFP_GPIO_NONE && cmod->mod_prsnt != SFP_GPIO_NONE)
	{
		// We assume we can use it.
		flags_ = SFP_FLAGS_SIG_SUPPORTED;
		if (gpio_get_level(cmod->rx_loss)) flags_ |= SFP_FLAGS_SIG_RX_LOSS;
		if (gpio_get_level(cmod->tx_fault)) flags_ |= SFP_FLAGS_SIG_TX_FAULT;
		if (gpio_get_level(cmod->mod_prsnt)) flags_ |= SFP_FLAGS_SIG_MOD_PRESENT;
	} else {
		// Otherwise, not.
		flags_ = 0;
	}
	*flags = flags_;

	return SFP_STATUS_OK;
}
