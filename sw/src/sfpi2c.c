/*
 * sfpi2c.c
 *
 *  Created on: 4 jul. 2017
 *      Author: vincentb
 */

#include "sfpi2c_prov.h"

sfp_status_t sfp_i2c_trx(sfp_i2c_slave_t * slave, sfp_i2c_trx_t * trx)
{
	sfp_prov_i2c_t * pi2c = (sfp_prov_i2c_t *)slave->i2c;
	return pi2c->trx(pi2c, slave->addr, trx);
}


sfp_status_t sfp_i2c_mtx(sfp_i2c_t * i2c, sfp_i2c_mtx_op_t oper)
{
	sfp_prov_i2c_t * pi2c = (sfp_prov_i2c_t *)i2c;
	return pi2c->mtx(pi2c, oper);
}
