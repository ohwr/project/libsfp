/*
 * wrpc_config.h
 *
 *  Created on: 23 okt. 2017
 *      Author: vincentb
 */

#ifndef WRPC_CONFIG_H_
#define WRPC_CONFIG_H_

/**
 * WRPC configuration file
 */

// ===========================================================================
//  GPIO Configuration
// ===========================================================================
#define WB_GPIO_BASE_ADDRESS	0x00000000
#define WB_GPIO_PORT_COUNT		32

// ===========================================================================
//  I2C Configuration
// ===========================================================================
#define WB_I2C_COUNT			3

// WR_I2C_MASTERS is a list of WR_I2C_MASTER items defiend as:
//   WR_I2C_MASTER(INDEX, NAME, ADDRESS)
//   Where:
//     INDEX 	runs from 0 to WB_I2C_COUNT - 1
//     NAME  	is the device name (e.g "I2C0", "I2C1", etc..)
//     ADDRESS	is the base address of the I2C device
#define WR_I2C_MASTERS	\
	WR_I2C_MASTER(0, "I2C0", 0x00000000)	\
	WR_I2C_MASTER(1, "I2C1", 0x00000000)	\
	WR_I2C_MASTER(2, "I2C2", 0x00000000)

// ===========================================================================
//  SFP Configuration
// ===========================================================================

#define	SFP_COUNT			1

// SFP_SLOTS: Defines the SFP slots
//   SFP_SLOT(I2C_IDX, NAME, MOD_PRESENT_GPIO, RX_LOSS_GPIO, TX_FAULT_GPIO)
//   Where:
//     SFP_IDX 				Index of the I2C device
//     NAME					Human readable name of this slot
//     MOD_PRESENT_GPIO 	Module present GPIO, or -1 if it does not exist
//     RX_LOSS_GPIO 		RX loss GPIO, or -1 if it does not exist
//     TX_FAULT_GPIO		TX fault GPIO, or -1 if it does not exist
#define SFP_SLOTS	\
	SFP_SLOT(0, "SFP0", -1, -1, -1)

#endif /* WRPC_CONFIG_H_ */
