/*
 * wrpc_gpio.c
 *
 *  Created on: 23 okt. 2017
 *      Author: vincentb
 */

#include "sfpgpio.h"
#include "wrpc_config.h"

SFP_STUB sfp_status_t gpio_set_pinmode(sfp_gpio_pin_t pin, gpio_pinmode_t mode)
{
	if (pin >= WB_GPIO_PORT_COUNT) return SFP_STATUS_INVALID;

	// TODO

	return SFP_STATUS_OK;
}

SFP_STUB bool gpio_get_level(sfp_gpio_pin_t pin)
{
	if (pin >= WB_GPIO_PORT_COUNT) return false;

	// TODO

	return false;
}

SFP_STUB void gpio_set_level(sfp_gpio_pin_t pin, bool level)
{
	if (pin >= WB_GPIO_PORT_COUNT) return;
	// TODO
}
