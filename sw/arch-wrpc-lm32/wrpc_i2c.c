/*
 * wrpc_i2c.c
 *
 *  Created on: 23 okt. 2017
 *      Author: vincentb
 */

#include "sfpi2c_prov.h"

#include "wrpc_config.h"

sfp_status_t sfp_i2c_mtx_lm32wb(struct sfp_prov_i2c_s * pi2c, sfp_i2c_mtx_op_t oper)
{
	// for now, locking and unlocking is not suported.
	return SFP_STATUS_OK;
}

sfp_status_t sfp_i2c_trx_lm32wb(struct sfp_prov_i2c_s * pi2c, uint8_t adr, sfp_i2c_trx_t * trx)
{
	// TODO execute transaction
	return SFP_STATUS_OK;
}




#define WR_I2C_MASTER(IDX, NAME, OFFSET)		\
	{											\
		.pub = { .name = NAME },				\
		.trx = sfp_i2c_trx_lm32wb,				\
		.mtx = sfp_i2c_mtx_lm32wb,				\
		.user0 = {.v_ptr = ((void *)OFFSET) },	\
		.user1 = {.u32 = IDX },					\
	},

static sfp_prov_i2c_t _i2c_devs[WB_I2C_COUNT] = {
		WR_I2C_MASTERS
};
#undef WR_I2C_MASTER


SFP_STUB unsigned int sfp_i2c_count()
{
	return WB_I2C_COUNT;
}


SFP_STUB sfp_status_t sfp_i2c_get(unsigned int idx, sfp_i2c_t ** i2cdev)
{
	if (idx >= WB_I2C_COUNT) return SFP_STATUS_INVALID;

	*i2cdev = &_i2c_devs[idx].pub;

	return SFP_STATUS_OK;
}
