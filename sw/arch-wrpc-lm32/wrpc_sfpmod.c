/*
 * wrpc_sfpmod.c
 *
 *  Created on: 23 okt. 2017
 *      Author: vincentb
 */

#include "sfpmod_prov.h"

#include "wrpc_config.h"

/*
typedef struct sfp_prov_mod_s {
	sfp_mod_t  			pub;		//!< Public SFP module structure
	sfp_mod_ee_rd_f		ee_rd;		//!< Eeprom read
	sfp_mod_ee_wd_f		ee_wr;		//!< Eeprom write
	sfp_mod_sts_f		sts;		//!< Get status
	void * 				user;		//!< Custom data object.
} sfp_prov_mod_t;

typedef struct sfp_comp_mod_s {
	sfp_prov_mod_t		prov;		//!< Provider interface
	sfp_i2c_t *			i2c;		//!< I2C bus pointer
	sfp_mux_op_t		mux0;		//!< Mux level 0, if any
	sfp_mux_op_t		mux1;		//!< Mux level 1, if any
	sfp_gpio_pin_t      tx_fault;	//!< Tx fault GPIO, or SFP_GPIO_NONE
	sfp_gpio_pin_t      rx_loss;	//!< Rx loss GPIO, or SFP_GPIO_NONE
	sfp_gpio_pin_t      mod_prsnt;	//!< Module present GPIO, or SFP_GPIO_NONE
	sfp_gpio_pin_t		reserved;	//!< Reserved
} sfp_comp_mod_t;

*/


#define SFP_SLOT(I2C_IDX, NAME, MOD_PRESENT_GPIO, RX_LOSS_GPIO, TX_FAULT_GPIO)	\
	{									\
	.prov = {							\
		.pub = { .name = NAME },		\
		.ee_rd = sfp_comp_mod_ee_rd,	\
		.ee_wr = sfp_comp_mod_ee_wr,	\
		.sts   = sfp_comp_mod_sts,		\
		.user = { .u32 = I2C_IDX } 		\
	},									\
	.i2c = NULL, /* lazy init */		\
	.mux0 = { .val = 0 },				\
	.mux1 = { .val = 0 },				\
	.tx_fault = TX_FAULT_GPIO,			\
	.rx_loss  = RX_LOSS_GPIO,			\
	.mod_prsnt = MOD_PRESENT_GPIO		\
	},

// we'll need to initialize this dynamically
static sfp_comp_mod_t _modules[SFP_COUNT] = {
		SFP_SLOTS
};

#undef SFP_SLOT

SFP_STUB unsigned int sfp_mod_count()
{
	return SFP_COUNT;
}

/*!
 * \brief Get the reference to a module at the specified index position.
 *
 * \param	idx		The index position,  0..sfp_mod_count() - 1 .
 * \param	module	A pointer pointer which will be set to provider module
 *
 * \return	A SFP_STATUS_* value. See \link sfp_status_t \endlink
 */
SFP_STUB sfp_status_t sfp_mod_get(unsigned int idx, sfp_mod_t ** module)
{
	if (idx >= SFP_COUNT) return SFP_STATUS_INVALID;

	// lazy init of I2C, as we can not call this at initialization time
	if (_modules[idx].i2c == NULL)
	{
		sfp_i2c_t * i2c;
		if (sfp_i2c_get(_modules[idx].prov.user.u32, &i2c) != SFP_STATUS_OK)
			return SFP_STATUS_MODULE_CONFIG_ERROR;

		_modules[idx].i2c = i2c;
	}

	*module = &_modules[idx].prov.pub;

	return SFP_STATUS_OK;
}
