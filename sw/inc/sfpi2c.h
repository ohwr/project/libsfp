/*!
 * \file	sfpi2c.h
 *
 * \brief	User header file for I2C.
 *
 * \author	Vincent van Beveren (v.van.beveren[at]nikhef.nl)
 * \date	4 July 2017
 */
#ifndef SFPI2C_H_
#define SFPI2C_H_

#include "sfpcommon.h"
#include <stdint.h>
#include <stddef.h>


/*!
 * \brief Generic I2C structure.
 */
typedef struct sfp_i2c_s {
	const char * name;		//!< I2C master name.
} sfp_i2c_t;

/*!
 * I2C slave definition.
 */
typedef struct sfp_i2c_slave_s {
	sfp_i2c_t * i2c;		//!< I2C master device
	uint8_t     addr;		//!< I2C slave address
} sfp_i2c_slave_t;


/*!
 * \brief Define an I2C slave (I2C device + address)
 *
 * @param	i2c 	I2C device
 * @param	addr	Address
 */
static inline sfp_i2c_slave_t sfp_i2c_slave(sfp_i2c_t * i2c, uint8_t addr)
{
	sfp_i2c_slave_t slave;
	slave.i2c = i2c;
	slave.addr = addr;
	return slave;
}

/*!
 * \brief Returns the number of I2C master devices.
 */
SFP_STUB unsigned int sfp_i2c_count();

/*!
 * \brief Returns the I2C module at the given index.
 *
 * \param	idx		The index of the device to get range 0..sfp_i2c_count() - 1.
 * \param	i2cdev	A pointer pointer to get to the I2C device structure.
 *
 * \return	A SFP_STATUS_* value. See \link sfp_status_t \endlink.
 */
SFP_STUB sfp_status_t sfp_i2c_get(unsigned int idx, sfp_i2c_t ** i2cdev);

/*!
 * \brief I2C mutex operation.
 */
typedef enum sfp_i2c_mtx_op_e {
	SFP_I2C_MUX_LOCK,		//!< Lock the I2C bus for usage by this module
	SFP_I2C_MUX_UNLOCK		//!< Unlock it.
} sfp_i2c_mtx_op_t;

/*!
 * \brief	Lock or unlock the mutex
 *
 * \param	i2c	The I2C master to lock or unlock
 * \param	The operation, one of SFP_I2C_MUX_*
 *
 * \return	A SFP_STATUS_* value. See \link sfp_status_t \endlink.
 */
sfp_status_t sfp_i2c_mtx(sfp_i2c_t * i2c, sfp_i2c_mtx_op_t oper);


/**
 * \brief I2C transaction structure.
 *
 * There are four types of operations:
 *
 * - WRITE        - Write from buffer 0 to I2C bus.
 * - READ		  - Read into buffer from I2C bus.
 * - WRITE_READ	  - Write from the first buffer and read into the second
 * - WRITE_WRITE  - Write from the first buffer and write from the second.
 * - EXISTS       - For this operation no buffers need to be filled, just
 *                  checks if its possible to read from this device.
 *
 * The first two operations can be use for simple, non registered chips like muxes or ADCs.
 * The second two operations can be used for registered devices, in which the proper register or
 * address must first be written. For example, reading 16 bytes from EEPROM address 0xBEEF can be
 * done as  follows:
 *
 * @code
 * uint8_t addr[2] = { 0xBE, 0xEF };
 * uint8_t data[16];
 *
 * sfp_i2c_trx_t trx = {
 *    I2C_TRX_OPER_WRITE_READ,
 * 	  {
 * 	      { addr, sizeof(addr) },
 * 	      { data, sizeof(data) }
 * 	  }
 * }
 *
 * sfp_i2c_trx(I2C1, 0xAA, &trx);
 * @endcode
 *
 */
typedef struct sfp_i2c_trx_s {
	enum {
		I2C_TRX_OPER_READ,			//!< Read into buf[0]
		I2C_TRX_OPER_WRITE,			//!< Write from buf[0]
		I2C_TRX_OPER_WRITE_READ,	//!< Write from buf[0] and read into buf[1]
		I2C_TRX_OPER_WRITE_WRITE,	//!< Write from buf[0] and write from buf[1]
		I2C_TRX_OPER_EXISTS			//!< Simple existence check by checking for an ACK on read. No buffers used.
	} oper;							//!< Operation(s) to perform.
	struct  {
		uint8_t * ptr;				//!< Pointer to buffer to read or fill
		size_t    len;				//!< Length of buffer to read or fill
	} buf[2];						//!< Two buffers
} sfp_i2c_trx_t;

/*!
 *
 * \brief	Execute an I2C transaction.
 *
 * \see		sfp_i2c_trx_t
 *
 * \param	slave	The I2C slave
 * \param	trx		A pointer to a transaction object
 *
 * \return	A SFP_STATUS_* value. See \link sfp_status_t \endlink.
 */
sfp_status_t sfp_i2c_trx(sfp_i2c_slave_t * slave, sfp_i2c_trx_t * trx);

/*!
 * I2C memory device read with 8 bit address space.
 */
static inline sfp_status_t sfp_i2c_a8_rd(sfp_i2c_slave_t * slave, uint8_t reg, uint8_t * data, size_t count)
{
	sfp_i2c_trx_t trx = {
		I2C_TRX_OPER_WRITE_READ,
		{
			{&reg, 1 },
			{data, count }
		}
	};
	return sfp_i2c_trx(slave, &trx);
}

/**
 * I2C memory device write with 8 bit address space.
 */
static inline sfp_status_t sfp_i2c_a8_wr(sfp_i2c_slave_t * slave, uint8_t reg, uint8_t * data, size_t count)
{
	sfp_i2c_trx_t trx = {
		I2C_TRX_OPER_WRITE_READ,
		{
			{ &reg, 1 },
			{ data, count }
		}
	};
	return sfp_i2c_trx(slave, &trx);
}

static inline sfp_status_t sfp_i2c_wr(sfp_i2c_slave_t * slave, uint8_t * data, size_t count)
{
	sfp_i2c_trx_t trx = {
		I2C_TRX_OPER_WRITE,
		{
			{ data, count }
		}
	};
	return sfp_i2c_trx(slave, &trx);
}

#endif /* SFPI2C_H_ */
