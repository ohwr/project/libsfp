/*!
 * \file	sfpmod.h
 *
 * \brief	User header file for SFP modules.
 *
 * \author	Vincent van Beveren (v.van.beveren[at]nikhef.nl)
 * \date	4 July 2017
 */
#ifndef SFPMOD_H_
#define SFPMOD_H_

#include <stdint.h>
#include <stddef.h>
#include "sfpcommon.h"

/*!
 * \brief	SFP module device.
 */
typedef struct sfp_mod_s {
	const char * name;
} sfp_mod_t;

/*!
 * \brief Address space of SFP module.
 */
typedef enum sfp_as_e {
	AS_A0 	 = 0xA000,	//!< Address space A0, reg range 0-255
	AS_A2_LO = 0xA200,	//!< Address space A2, reg range 0-127
	AS_A2_P0 = 0xA280,	//!< Address space A2, page 0, reg range 128-255
	AS_A2_P1 = 0xA281,	//!< Address space A2, page 1, reg range 128-255
	AS_A2_P2 = 0xA282,	//!< Address space A2, page 2, reg range 128-255
	AS_A2_P3 = 0xA283,  //!< Address space A2, page 3, reg range 128-255
	AS_A2_P4 = 0xA284,  //!< Address space A2, page 4, reg range 128-255
	AS_A2_P5 = 0xA285   //!< Address space A2, page 5, reg range 128-255
} sfp_as_t;

/*!
 * @{
 * \name	SFP Flags
 * \brief 	Flags used by \link sfp_mod_status \endlink.
 */
#define SFP_FLAGS_SIG_SUPPORTED			0x01		//!< Signals supported
#define SFP_FLAGS_SIG_TX_FAULT			0x02		//!< TX fault signal is asserted
#define SFP_FLAGS_SIG_RX_LOSS			0x04		//!< RX loss signal is asserted
#define SFP_FLAGS_SIG_MOD_PRESENT		0x08		//!< Module present signal is asserted
/*! @} */

/*!
 * \brief Query the number of SFP bays.
 *
 * The number does not necessary require the module to be actually present. For an 18 port
 * switch this number always returns 18. Use the \link SFP_FLAGS_SIG_MOD_PRESENT \endlink signal
 * to check if the module is actually in the bay, if supported.
 *
 * \return	The number of possible SFP modules present
 */
SFP_STUB unsigned int sfp_mod_count();

/*!
 * \brief Get the reference to a module at the specified index position.
 *
 * \param	idx		The index position,  0..sfp_mod_count() - 1 .
 * \param	module	A pointer pointer which will be set to provider module
 *
 * \return	A SFP_STATUS_* value. See \link sfp_status_t \endlink
 */
SFP_STUB sfp_status_t sfp_mod_get(unsigned int idx, sfp_mod_t ** module);

/*!
 * \brief	Read the SFP eeprom/diagnostics data.
 *
 * \param	mod		The module
 * \param	aspace	The address space
 * \param	reg		The register
 * \param	data	The buffer to write the data into
 * \paran	count	The number of bytes to read
 *
 * \return	A SFP_STATUS_* value. See \link sfp_status_t \endlink
 */
sfp_status_t sfp_mod_ee_rd(sfp_mod_t * mod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count);

/*!
 * \brief	Write the SFP eeprom/diagnostics data.
 *
 * \param	mod		The module
 * \param	aspace	The address space
 * \param	reg		The register
 * \param	data	The buffer to read the data from
 * \paran	count	The number of bytes to write
 *
 * \return	A SFP_STATUS_* value. See \link sfp_status_t \endlink
 */
sfp_status_t sfp_mod_ee_wr(sfp_mod_t * mod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count);

/*!
 * \brief	Get module status flags.
 *
 * \param	mod		A pointer to the module
 * \param	flags	A pointer to an integer to get the flags onto.
 *
 * \return	A SFP_STATUS_* value. See \link sfp_status_t \endlink
 */
sfp_status_t sfp_mod_status(sfp_mod_t * mod, uint32_t * flags);


#endif /* SFPMOD_H_ */
