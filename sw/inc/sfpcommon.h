/*!
 * \file	sfpcommon.h
 *
 * \brief	Common types and structures
 *
 * \author	Vincent van Beveren v.van.beveren[at]nikhef.nl
 * \date	4 July 2017
 */

#ifndef SFPCOMMON_H_
#define SFPCOMMON_H_

#include <stdint.h>

//! Marker to indicate the function needs to be implemented by the architecture.
#define SFP_STUB


/*!
 * \brief Return status
 */
typedef enum sfp_status_e {
	SFP_STATUS_OK,
	SFP_STATUS_ERROR,					//!< Generic unspecifeid error
	SFP_STATUS_INVALID,					//!< Resource is invalid
	SFP_STATUS_NOT_SUPPORTED,			//!< Not supported operation
	SFP_STATUS_MODULE_CONFIG_ERROR,		//!< Some part of the module configuration is invalid
	SFP_STATUS_RESOURCE_IN_USE			//!< Can't execute operation, resource in use
} sfp_status_t;


/**
 * Generic variant type object.
 */
typedef union sfp_variant_u {
	void *			v_ptr;
	uint32_t		u32;
} sfp_variant_t;

#endif /* SFPCOMMON_H_ */
