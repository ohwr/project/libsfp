/**
 * @file
 */

#ifndef SFPMOD_PROV_H_
#define SFPMOD_PROV_H_

#include "sfpmod.h"

#include "sfpi2c.h"
#include "sfpgpio.h"

#include <stdint.h>
#include <stddef.h>

// -----------------------------------------------------------------------------------------------------
//  Generic macros
// -----------------------------------------------------------------------------------------------------
// Address space operations
#define SFP_AS2ADDR(AS)		( (AS) >> 8 )
#define SFP_AS_PAGEMASK		0x0080
#define SFP_AS2PAGE(AS)		( (AS) & 0x7F )

// -----------------------------------------------------------------------------------------------------
//  Architecture stub functions
// -----------------------------------------------------------------------------------------------------

/**
 * Stub functions must be implemented in the architecture code.
 */
sfp_status_t _sfp_mod_get(unsigned int idx, sfp_mod_t ** module);
unsigned int _sfp_mod_count();


// -----------------------------------------------------------------------------------------------------
//  High level provider interface.
// -----------------------------------------------------------------------------------------------------

/**
 * The high level provider interface is the highest level on which you can implement SFP access functions.
 *
 *
 * There are three main functions to implement:
 *   ee_rd -> Eeprom read function
 *   ee_wr -> eeprom write function
 *   sts   -> Module status (usually from GPIO)
 *
 * Implementation is entirely up to the implementor.
 *
 * It is also possible to use a ready-made provider, which requires a i2c and gpio abstraction. See
 * the composite provider below.
 */

struct sfp_prov_mod_s;

typedef sfp_status_t (*sfp_mod_ee_rd_f)(struct sfp_prov_mod_s * pmod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count);
typedef sfp_status_t (*sfp_mod_ee_wr_f)(struct sfp_prov_mod_s * pmod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count);
typedef sfp_status_t (*sfp_mod_sts_f  )(struct sfp_prov_mod_s * pmod, uint32_t * flags);

/**
 * Provider interface.
 */
typedef struct sfp_prov_mod_s {
	sfp_mod_t  			pub;		//!< Public SFP module structure
	sfp_mod_ee_rd_f		ee_rd;		//!< Eeprom read
	sfp_mod_ee_wr_f		ee_wr;		//!< Eeprom write
	sfp_mod_sts_f		sts;		//!< Get status
	sfp_variant_t		user;		//!< Custom data object.
} sfp_prov_mod_t;


// -----------------------------------------------------------------------------------------------------
//  Composite provider interface
// -----------------------------------------------------------------------------------------------------

#define SFP_MUX_TYP_NONE			0x00		//!< There is no mux
#define SFP_MUX_TYP_I2C				0x01		//!< Its a I2C mux

#define SFP_MUX_PART_PCA9548		0x01		//!< PCA9548 I2C mux

/*!
 * Mux definition union.
 *
 * Type defines the argument. If type is I2C, then the i2c union should be used.
 *
 * Currently only the PCA9548 is supported, but I'm pretty sure there are many alike.
 */
typedef union sfp_mux_op {
	struct {
		uint8_t typ;
		uint8_t __resv[3];
	} any;
	struct {
		uint8_t typ;
		uint8_t part;
		uint8_t addr;
		uint8_t pos;
	} i2c;
	// optional other mux types (GPIO?)

	uint32_t val;		// for simple 0 assignment
} sfp_mux_op_t;

/*!
 * Composite SFP MOD structure.
 *
 * When accessing an EEPROM register, the following sequence is executed:
 *
 * 1) The I2C mutex lock function is called
 * 2) The first and second mux operation are executed, if set
 * 3) The i2c read/write operation is executed in the provider register
 * 4) The I2C mutex is released
 */
typedef struct sfp_comp_mod_s {
	sfp_prov_mod_t		prov;		//!< Provider interface
	sfp_i2c_t *			i2c;		//!< I2C bus pointer
	sfp_mux_op_t		mux0;		//!< Mux level 0, if any
	sfp_mux_op_t		mux1;		//!< Mux level 1, if any
	sfp_gpio_pin_t      tx_fault;	//!< Tx fault GPIO, or SFP_GPIO_NONE
	sfp_gpio_pin_t      rx_loss;	//!< Rx loss GPIO, or SFP_GPIO_NONE
	sfp_gpio_pin_t      mod_prsnt;	//!< Module present GPIO, or SFP_GPIO_NONE
	sfp_gpio_pin_t		reserved;	//!< Reserved
} sfp_comp_mod_t;

sfp_status_t sfp_comp_mod_ee_rd(struct sfp_prov_mod_s * pmod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count);
sfp_status_t sfp_comp_mod_ee_wr(struct sfp_prov_mod_s * pmod, sfp_as_t aspace, uint8_t reg, uint8_t * data, size_t count);
sfp_status_t sfp_comp_mod_sts  (struct sfp_prov_mod_s * pmod, uint32_t * flags);

#endif /* SFPMOD_PROV_H_ */
