/*!
 * \file	sfpgpio.h
 *
 * \brief	Public GPIO header file
 *
 * \author	Vincent van Beveren (v.van.beveren[at]nikhef.nl)
 * \date	4 July 2017
 *
 * Currently there is no notion of a GPIO instance in this abstraction. Usually there is just one GPIO
 * instance. Otherwise one should use the upper bits in the pin value to make the distinction.
 */

#ifndef SFPGPIO_H_
#define SFPGPIO_H_

#include "sfpcommon.h"
#include <stdint.h>
#include <stdbool.h>

/**
 * Pin-mode configuration.
 */
typedef enum gpio_pinmode_e {
	GPIO_PINMODE_INPUT,
	GPIO_PINMODE_OUTPUT
} gpio_pinmode_t;


/**
 * Definition of a GPIO pin
 */
typedef uint16_t sfp_gpio_pin_t;

/**
 * PIN error.
 */
#define SFP_GPIO_NONE	((sfp_gpio_pin_t)0xFFFF)

/**
 * Sets the pin mode.
 *
 * @param	pin			The pin
 * @param	mode		The mode
 *
 *
 * @retval SFP_STATUS_OK				If all is Ok
 * @retval SFP_STATUS_NOT_SUPPORTED		If the the mode does not exist
 * @retval SFP_STATUS_INVALID			If the pin is does not exist.
 */
SFP_STUB sfp_status_t gpio_set_pinmode(sfp_gpio_pin_t pin, gpio_pinmode_t mode);

/**
 * Get GPIO pin level.
 *
 * If this is an input, it is the level the pin reads.
 * If this is an output, it is the level the pin was set on.
 *
 * If the pin does not exist, this returns false.
 *
 * @param	pin			The pin
 *
 * @retval	true		Pin level high
 * @retval	false		Pin level low
 */
SFP_STUB bool gpio_get_level(sfp_gpio_pin_t pin);

/**
 * Set GPIO pin level. Only useful for output.
 *
 * Function does nothing if pin does not exist.
 *
 * @param	pin			The pin
 * @param	level		The level to set: true - high, false - low.
 */
SFP_STUB void gpio_set_level(sfp_gpio_pin_t pin, bool level);



#endif /* SFPGPIO_H_ */
