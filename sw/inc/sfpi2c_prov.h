/*!
 * \file	sfpi2c_prov.h
 *
 * \brief	Architecture providers implementors header file for I2C.
 *
 * \author	Vincent van Beveren (v.van.beveren[at]nikhef.nl)
 * \date	4 July 2017
 */
#ifndef SFPI2C_PROV_H_
#define SFPI2C_PROV_H_

#include "sfpi2c.h"

/*!
 * Provider I2C structure.
 */
struct sfp_prov_i2c_s;

/*!
 * Callback to implement for locking and unlocking the I2C bus.
 *
 * \param	pi2c		The provider I2C interface.
 * \param	oper		The operation to perform (LOCK/UNLOCK)
 *
 * \return	The sfp_status value, one of SFP_STATUS_*
 */
typedef sfp_status_t (*sfp_i2c_mtx_f)(struct sfp_prov_i2c_s * pi2c, sfp_i2c_mtx_op_t oper);


/*!
 * Callback to implement for executing a transaction.
 *
 * \param	pi2c		The provider I2C interface.
 * \param	adr			The I2C device address, R/w bit set to 0.
 * \param	trx			Transaction object pointer.
 *
 * \return	The sfp_status value, one of SFP_STATUS_*
 */
typedef sfp_status_t (*sfp_i2c_trx_f)(struct sfp_prov_i2c_s * pi2c, uint8_t adr, sfp_i2c_trx_t * trx);

/*!
 * Private provided stub.
 */
typedef struct sfp_prov_i2c_s {
	sfp_i2c_t  			pub;		//!< Public SFP module structure
	sfp_i2c_trx_f		trx;		//!< I2C transaction
	sfp_i2c_mtx_f		mtx;		//!< I2C mutex
	sfp_variant_t		user0;		//!< User object 0
	sfp_variant_t		user1;		//!< User object 1
} sfp_prov_i2c_t;


#endif /* SFPI2C_PROV_H_ */
